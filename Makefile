#! /usr/bin/make -f

.PHONY: cache-data
cache-data:
	python3 cache-data.py

.PHONY: convert-data
convert-data:
	python3 convert-data.py

.PHONY: tests
tests:
	pytest-3

# run tests not requiring network
.PHONY: quicktests
quicktests:
	pytest-3 -m 'not network'

.PHONY: lint
lint:
	flake8 . bin/*

.PHONY: clean
clean:
	find . -type f -name '*.pyc' -delete
	find . -type d -name '__pycache__' -delete
	rm -f reportbug/data.py
	rm -rf build
	rm -rf reportbug.egg-info
