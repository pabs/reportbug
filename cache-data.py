#!/usr/bin/python3


# cache-data.py - caches data that is maintained elsewhere
#
#   Copyright 2023-2025 Paul Wise <pabs@debian.org>
#
# This program is freely distributable per the following license:
#
#  Permission to use, copy, modify, and distribute this software and its
#  documentation for any purpose and without fee is hereby granted,
#  provided that the above copyright notice appears in all copies and that
#  both that copyright notice and this permission notice appear in
#  supporting documentation.
#
#  I DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING ALL
#  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT SHALL I
#  BE LIABLE FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY
#  DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
#  WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,
#  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS
#  SOFTWARE.


from pprint import pformat
import psycopg
import requests


warning = "DOWNLOADED AND CACHED DATA - DO NOT EDIT - see README for more info"


def download_arch_usertags_table():
    arch_usertags_table = []
    wiki = requests.get('https://wiki.debian.org/Teams/Debbugs/ArchitectureTags?action=raw')
    for line in wiki.text.split('\r\n'):
        if line.startswith('|| ') and not line.startswith("|| '''"):
            arch_usertags_table.append(line)
    return '\n'.join(arch_usertags_table)


def download_arch_names_wml():
    arch_names_wml = []
    add_line = False
    webwml = requests.get('https://salsa.debian.org/webmaster-team/webwml/-/raw/master/english/releases/arches.data')
    for line in webwml.text.split('\n'):
        line = line.strip()
        if line == '%arches = (':
            add_line = True
            continue
        if add_line and line == ');':
            break
        if add_line:
            arch_names_wml.append(line)
    return '\n'.join(arch_names_wml)


def download_wannabuild_arches():
    with psycopg.connect('postgresql://udd-mirror:udd-mirror@udd-mirror.debian.net/udd') as conn:
        with conn.cursor() as cur:
            cur.execute('SELECT DISTINCT architecture, vancouvered FROM wannabuild ORDER BY architecture;')
            archs = cur.fetchall()
            archs = [(arch.decode(), unofficial) for arch, unofficial in archs]
            return archs


def cache(filename, data):
    with open('cache/' + filename, 'w') as f:
        print('#', warning, file=f)
        print(file=f)
        print(data, file=f)


def download_cache_architecture_info():
    cache('wiki-Teams-Debbugs-ArchitectureTags-usertags-table.moin', download_arch_usertags_table())
    cache('salsa-webmaster-team-webwml-releases-arches-only.data', download_arch_names_wml())
    cache('udd-wannabuild-architecture-vancouvered.py', pformat(download_wannabuild_arches()))


download_cache_architecture_info()
